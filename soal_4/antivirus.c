#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

void isFolder(){
    struct stat st = {0};
    const char *folderName = "sisop_infected";

    if (stat(folderName, &st) == -1) {
        mkdir(folderName, 0777);
    }
}

void downloadFile(){
    char folder[100];
    snprintf(folder, sizeof(folder), "%s/%s", getcwd(NULL, 0), "sisop_infected");

    chdir(folder);

    char *argv[] = {"curl", "-L", "-o", "extension.csv", "https://drive.google.com/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", NULL};
        
    execvp("curl", argv);
    perror("execvp");
    exit(1);
}

void saveFile(){
    isFolder();

    if (access("sisop_infected/extension.csv", F_OK) == -1){
        int status;
        pid_t pid = fork();

        if (pid == 0){
            downloadFile();
            exit(0);
        } else if (pid < 0){
            perror("There is an error during fork.");
            exit(1);
        } else {
            waitpid(pid, &status, 0);
            if (WEXITSTATUS(status) != 0){
                fprintf(stderr, "Failed to download file.\n ");
                exit(1);
            }
        }
    }
}

void createLog(const char *line, const char *action){
    time_t now;
    struct tm *currentTime;

    time(&now);
    currentTime = localtime(&now);

    char logTime[25];
    strftime(logTime, sizeof(logTime), "%d-%m-%y:%H-%M-%S", currentTime);

    FILE *fileOfLog = fopen("virus.log", "a");
    if (fileOfLog == NULL){
        perror("Error while opening log file.");
        exit(1);
    }

    char *userName = getenv("USER");
    fprintf(fileOfLog, "[%s][%s] - %s - %s\n", userName, logTime, line, action);

    fclose(fileOfLog);
}

void decryptFile(){
    FILE *undecrypt = fopen("sisop_infected/extension.csv", "r");
    FILE *decrypt = fopen("sisop_infected/decrypted.csv", "w");

    if (undecrypt == NULL || decrypt == NULL){
        perror("Error while opening file.");
        exit(1);
    }

    char line[4096];
    int lineNumber = 0;

    while (fgets(line, sizeof(line), undecrypt)){
        lineNumber++;

        if (lineNumber <= 8) {
            continue;
        }

        for(int i = 0; i < strlen(line); i++){
            if ('A' <= line[i] && line[i] <= 'Z'){
                line[i] = ((line[i] - 'A' + 13) % 26) + 'a';
            } else if ('a' <= line[i] && line[i] <= 'z'){
                line[i] = ((line[i] - 'a' + 13) % 26) + 'a';
            }
        }

        fprintf(decrypt, "%s", line);
    }
}

void checkFiles(){

}

int main(int argc, char const *argv[])
{
    saveFile();
    decryptFile();
    return 0;
}
