#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

void createLog(const char *msg){
    char pathOfLog[512];
    snprintf(pathOfLog, sizeof(pathOfLog), "%s/cleaner.log", getenv("HOME"));

    FILE *theLog = fopen(pathOfLog, "a");

    if (theLog != NULL){
        time_t currentTime = time(NULL);
        struct tm *createdLog = localtime(&currentTime);
        char logTime[20];
        strftime(logTime, sizeof(logTime), "%Y-%m-%d %H:%M:%S", createdLog);

        fprintf(theLog, "[%s] %s\n", logTime, msg);
        fclose(theLog);
    }
}

void removeSus(const char *pathOfFile){
    if (remove(pathOfFile) == 0){
        char logMsg[1024];
        snprintf(logMsg, sizeof(logMsg), "'%s' has been removed.", pathOfFile);
        createLog(logMsg);
    }
}

void openFile(const char *folder, const char *fileName){
    char pathOfFile[512];
    snprintf(pathOfFile, sizeof(pathOfFile), "%s/%s", folder, fileName);

    FILE *filee = fopen(pathOfFile, "r");
    if (filee != NULL){
        char hapus[512];
        while (fgets(hapus, sizeof(hapus), filee) != NULL){
            if (strstr(hapus, "SUSPICIOUS") != NULL){
                removeSus(pathOfFile);
                break;
            }
        }
    }
}

void openFolder(const char *folder){
    DIR *isFolder = opendir(folder);
    if(isFolder == NULL){
        perror("Error: Wrong path of directory.");
        exit(EXIT_FAILURE);
    }

    struct dirent *enter;

    while ((enter = readdir(isFolder)) != NULL){
        if (enter->d_type == DT_REG){
            openFile(folder, enter->d_name);
        }
    }
}

int main(int argc, char *argv[]) {

    if (argc != 2) { 
        fprintf(stderr, "Error: No path entered.\nPlease enter command like: %s <path>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    signal(SIGTERM, exit);

    pid_t pid = fork();
    if (pid < 0){
        perror("There is an error during fork");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    
    if(setsid() < 0){
        perror("There is an error during setsid");
        exit(EXIT_FAILURE);
    }

    signal(SIGCHLD, SIG_IGN);

    chdir("/");

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    umask(0);

    while (1)
    {
        openFolder(argv[1]);

        sleep(30);
    }

    return 0;
}