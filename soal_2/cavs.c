#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/wait.h>
#include <errno.h>

void createPlayersFolder() {
    mkdir("players", 0777);
}

void downloadPlayerDatabase() {
    int status;
    pid_t pid11 = fork();
    if (pid11 == 0) {
        char *argv[] = {"curl", "-L", "-o", "players.zip", "https://drive.google.com/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", NULL};
        execv("/usr/bin/curl", argv);
        perror("execv");
        exit(1);
    } else if (pid11 < 0) {
        perror("fork");
        exit(1);
    } else {
        waitpid(pid11, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Failed to download player database.\n");
            exit(1);
        }
    }
}

void extractZipFile(char *zipFileName, char *destFolder) {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", zipFileName, "-d", destFolder, NULL);
        perror("execl");
        exit(1);
    } else if (pid < 0) {
        perror("fork");
        exit(1);
    } else {
        wait(NULL);
    }
}

void removePlayers(char *directory) {
    struct dirent *dp;
    DIR *folder;
    folder = opendir(directory);

    if (folder != NULL) {
        while ((dp = readdir(folder)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL) {
                if (dp->d_type == DT_REG) {
                    if (strstr(dp->d_name, "Cavaliers") == NULL) {
                        char path[512];
                        sprintf(path, "%s/%s", directory, dp->d_name);

                        pid_t child_id = fork();
                        if (child_id == 0) {
                            char *args[] = {"rm", "-f", path, NULL};
                            execv("/bin/rm", args);
                            perror("execv");
                            exit(1);
                        } else if (child_id < 0) {
                            perror("fork");
                            exit(1);
                        } else {
                            int status;
                            waitpid(child_id, &status, 0);
                            if (WEXITSTATUS(status) != 0) {
                                fprintf(stderr, "Failed to remove file: %s\n", path);
                            }
                        }
                    }
                }
            }
        }
        closedir(folder);
    }
}


void categorizePlayers() {
    const char* srcDir = "players";
    DIR* d = opendir(srcDir);

    if (!d) {
        perror("Error opening directory");
        return;
    }

    struct dirent* dir;
    while ((dir = readdir(d)) != NULL) {
        const char* filename = dir->d_name;
        if (strstr(filename, "Cavaliers") != NULL && strstr(filename, ".png") != NULL) {
            char position[10];
            if (sscanf(filename, "Cavaliers-%2s", position) != 1) {
                continue;
            }
            if (strcmp(position, "C-") == 0) {
                strcpy(position, "C");
            }

            char position_folder[32];
            snprintf(position_folder, sizeof(position_folder), "players/%s", position);

            if (mkdir(position_folder, 0777) == -1 && errno != EEXIST) {
                perror("Error creating folder");
                continue;
            }

            char srcPath[512];
            char destPath[512];
            snprintf(srcPath, sizeof(srcPath), "%s/%s", srcDir, filename);
            snprintf(destPath, sizeof(destPath), "%s/%s", position_folder, filename);

            if (rename(srcPath, destPath) == -1) {
                perror("Error moving file");
            }
        }
    }

    closedir(d);
}

void generateFormationFile() {
    int counts[5] = {0};


    char positions[5][3] = {"PG", "SG", "SF", "PF", "C"};

    for (int i = 0; i < 5; i++) {
        char position_folder[32];
        snprintf(position_folder, sizeof(position_folder), "players/%s", positions[i]);
        DIR *d = opendir(position_folder);
        if (d) {
            struct dirent *dir;
            while ((dir = readdir(d)) != NULL) {
                // Periksa jika file sesuai dengan kriteria
                if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                    counts[i]++;
                }
            }
            closedir(d);
        }
    }

    FILE *formation_file = fopen("Formasi.txt", "w");
    if (formation_file) {
        for (int i = 0; i < 5; i++) {
            fprintf(formation_file, "%s: %d\n", positions[i], counts[i]);
        }
        fclose(formation_file);
    } else {
        perror("Error creating formation file");
    }
}

void createClutchFolder() {
    mkdir("clutch", 0777);
}

void moveClutchPhoto() {
    char src[512];
    char end[512];
    char *  Clutch = "Cavaliers-PG-Kyrie-Irving.png";
    snprintf(src, sizeof(src), "players/PG/%s", Clutch);
    snprintf(end, sizeof(end), "clutch/%s", Clutch);

    int success = rename(src, end);
    if (success == 0) {
        printf("Moved %s to clutch.\n", Clutch);
    } else {
        perror("Error moving clutch photo");
    }
}

void moveTheBlockPhoto() {
    char src[512];
    char end[512];
    char *  TheBlock = "Cavaliers-SF-LeBron-James.png";
    snprintf(src, sizeof(src), "players/SF/%s", TheBlock);
    snprintf(end, sizeof(end), "clutch/%s", TheBlock);

    int success = rename(src, end);
    if (success == 0) {
        printf("Moved %s to clutch.\n", TheBlock);
    } else {
        perror("Error moving clutch photo");
    }
}


int main() {
    createPlayersFolder();
    downloadPlayerDatabase();
    extractZipFile("players.zip", "players");
    remove("players.zip");

    removePlayers("players");
    categorizePlayers();
    generateFormationFile();

    createClutchFolder();
    moveClutchPhoto();
    moveTheBlockPhoto();

    return 0;
}


